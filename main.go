package main

import (
	"fmt"
	"log"
	"main/route"
	"net/http"
)

func main() {
	router := route.NewRouter()
	fmt.Println("SYSTEM START")
	log.Fatal(http.ListenAndServe(":80", router))
}
