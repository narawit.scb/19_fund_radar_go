package handler

import (
	"database/sql"
	"main/model"
)


func selectAllFund(db *sql.DB) (model.Funds, error){
	var fund model.Fund
	var funds model.Funds

	sql := "SELECT mstar_id, fund_code, fund_name, provider_name, nav, aum, re_1d, re_1w, re_1m, re_ytd, re_1y, re_3y, aum_1m, val_1m, std_1y, is_ltf, is_rmf, is_equity" +
		" FROM  public.fund_performance LIMIT 100"

	rows, err := db.Query(sql)

	if err != nil {
		return funds, err
	}

	for rows.Next() {
		err = rows.Scan(&fund.ID, &fund.Code, &fund.Name, &fund.Provider, &fund.NAV, &fund.AUM, &fund.Re1D, &fund.Re1W, &fund.Re1M, &fund.ReYTD, &fund.Re1Y, &fund.Re3Y, &fund.A1M, &fund.Buy1M, &fund.STD1Y, &fund.IsLTF, &fund.IsRMF, &fund.IsEquity)
		funds = append(funds, fund)
	}

	if err != nil {
		return model.Funds{}, err
	}

	return funds,nil
}
