package handler

import (
	"database/sql"
	"log"
	"main/driver"
	"main/model"
	"main/utils"
	"net/http"
)

var (
	db *sql.DB
)

func init(){
	db = driver.ConnectDB()
}

func GetAllFund(w http.ResponseWriter, r *http.Request){
	log.Println("Get all funds is called")
	funds, err := selectAllFund(db)
	if err != nil {
		log.Fatal(err)
		error := model.Error{"Database error"}
		utils.SendError(w, http.StatusNotFound, error)
	}
	utils.SendSuccess(w, funds)
}
