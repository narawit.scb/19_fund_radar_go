package driver

import (
	"database/sql"
	"github.com/lib/pq"
	"log"
	"main/utils"
)

var db *sql.DB

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func ConnectDB() *sql.DB {

	pgUrl, err := pq.ParseURL(utils.LoadConfiguration().DBUrl)
	logFatal(err)

	db, err = sql.Open("postgres", pgUrl)
	logFatal(err)

	err = db.Ping()
	logFatal(err)

	return db
}