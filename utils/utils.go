package utils

import (
	"encoding/json"
	"github.com/tkanos/gonfig"
	"main/model"
	"net/http"
)

type Config struct {
	Port	int
	DBUrl 	string
}

func LoadConfiguration() Config {
	configuration := Config{}
	err := gonfig.GetConf("config.json", &configuration)
	if err != nil {
		panic(err)
	}
	return configuration
}


func SendError(w http.ResponseWriter, status int, err model.Error) {
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(err)
}

func SendSuccess(w http.ResponseWriter, data interface{}) {
	json.NewEncoder(w).Encode(data)
}