FROM golang:1.12

LABEL maintainer="Narawit Chararin <narawit.c@ku.th>"

# Set working directory to main app
WORKDIR $GOPATH/src/main

COPY . .

#TODO Change to available port
EXPOSE 80

CMD ["./main"]

