module main

go 1.12

require (
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/gorilla/mux v1.7.2
	github.com/lib/pq v1.1.1
	github.com/tkanos/gonfig v0.0.0-20181112185242-896f3d81fadf
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
