package route

import (
	"github.com/gorilla/mux"
	"main/handler"
)


func addFundRoute(router *mux.Router){

	router.HandleFunc("/funds", handler.GetAllFund).Methods("GET")

}
