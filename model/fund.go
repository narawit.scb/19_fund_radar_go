package model

type Fund struct {
	ID string `json:id`
	Code string `json:code`
	Name string `json:name`
	Provider string `json:provider`
	NAV float64 `json:nav`
	AUM float64 `json:aum`
	Re1D float64 `json:re_1d`
	Re1W float64 `json:re_1w`
	Re1M float64 `json:re_1m`
	ReYTD float64 `json:re_ytd`
	Re1Y float64 `json:re_1y`
	Re3Y float64 `json:re_3y`
	A1M float64 `json:aum_1m`
	Buy1M float64 `json:val_1m`
	STD1Y float64 `json:std_1y`
	IsLTF int `json:is_ltf`
	IsRMF int `json:is_rmf`
	IsEquity int `json:is_equity`
}

type Funds []Fund
